# Imporatation de la module csv et unicode
import csv
import unicodedata

list_mdp = []

def modif_nom(nom):
    # Normalisation des caracteres spéciaux
    nom = unicodedata.normalize("NFKD",nom).encode('ascii','ignore').decode('utf-8')
    # Remplacement des espaces, tirets etc...
    nom = ''.join(ch for ch in nom if ch.isalpha() and ch.isascii()).replace('-','').replace(' ','')
    return nom

# Lecture des utilisateurs depuis usersToulouse.csv et création des login.
with open('usersToulouse.csv', 'r', newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    list_login = []
    for row in reader:
        prenom = row['givenName'].split('-')
        nom = row['cn']
        # Traiter les prénoms composé
        initials = ''.join(prenom[0] for prenom in prenom)
        # Création des logins
        login = (initials + modif_nom(nom)).lower()
        list_login.append(login)
        
# Lecture des mot de passes créer depuis usersPassword.csv et insertion dans une liste.
with open('usersPassword2.csv','r',newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    for row in reader:
        list_mdp.append(row['password'])
        
# Écriture des instructions SQL dans un fichier SQL.
with open('creerUsersBddAcces.sql', 'w') as sqlfile:
    # Ecriture des requetes SQL pour création d'utilisateur.
    for idx, user in enumerate(list_login):
        login = user
        mdp = list_mdp[idx]
        # Création de la base de données
        sqlfile.write(f"CREATE DATABASE IF NOT EXISTS db{login} CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;\n")
        # Création de l'utilisateur
        sqlfile.write(f"CREATE USER '{login}'@'localhost' IDENTIFIED BY '{mdp}';\n")
        # Donner les privileges
        sqlfile.write(f"GRANT CREATE, ALTER, DROP, INSERT, UPDATE, DELETE, SELECT ON db{login}.* TO '{login}'@'localhost';\n")