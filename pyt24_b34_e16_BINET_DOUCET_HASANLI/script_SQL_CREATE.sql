CREATE DATABASE IF NOT EXISTS dbcruiz CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER 'cruiz'@'localhost' IDENTIFIED BY 'gW4$:96f7VH+';
GRANT ALL PRIVILEGES ON dbcruiz.* TO 'cruiz'@'localhost';
CREATE DATABASE IF NOT EXISTS dbnsamson CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER 'nsamson'@'localhost' IDENTIFIED BY '5%>FAI0iqz9H';
GRANT ALL PRIVILEGES ON dbnsamson.* TO 'nsamson'@'localhost';
CREATE DATABASE IF NOT EXISTS dbmbonhomme CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER 'mbonhomme'@'localhost' IDENTIFIED BY '!8%t5Zw=a.Ip';
GRANT ALL PRIVILEGES ON dbmbonhomme.* TO 'mbonhomme'@'localhost';
CREATE DATABASE IF NOT EXISTS dbaleleu CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER 'aleleu'@'localhost' IDENTIFIED BY 'EtHu5b3Nx7.#';
GRANT ALL PRIVILEGES ON dbaleleu.* TO 'aleleu'@'localhost';
CREATE DATABASE IF NOT EXISTS dbebriere CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER 'ebriere'@'localhost' IDENTIFIED BY '16E?XCq8mBL=';
GRANT ALL PRIVILEGES ON dbebriere.* TO 'ebriere'@'localhost';
CREATE DATABASE IF NOT EXISTS dbcauvray CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER 'cauvray'@'localhost' IDENTIFIED BY 'FMy@$9d0p9Td';
GRANT ALL PRIVILEGES ON dbcauvray.* TO 'cauvray'@'localhost';
CREATE DATABASE IF NOT EXISTS dbnpage CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER 'npage'@'localhost' IDENTIFIED BY 'Hl$o5c%R9PMF';
GRANT ALL PRIVILEGES ON dbnpage.* TO 'npage'@'localhost';
CREATE DATABASE IF NOT EXISTS dbmportier CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER 'mportier'@'localhost' IDENTIFIED BY '2u1-6BwEk/7C';
GRANT ALL PRIVILEGES ON dbmportier.* TO 'mportier'@'localhost';
CREATE DATABASE IF NOT EXISTS dbavergne CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER 'avergne'@'localhost' IDENTIFIED BY 'k24i*KmGb+1@';
GRANT ALL PRIVILEGES ON dbavergne.* TO 'avergne'@'localhost';
CREATE DATABASE IF NOT EXISTS dbmbernard CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER 'mbernard'@'localhost' IDENTIFIED BY 'J#:j2&+3PSBg';
GRANT ALL PRIVILEGES ON dbmbernard.* TO 'mbernard'@'localhost';
CREATE DATABASE IF NOT EXISTS dbamichel CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER 'amichel'@'localhost' IDENTIFIED BY 'k$R:08E>Jb0q';
GRANT ALL PRIVILEGES ON dbamichel.* TO 'amichel'@'localhost';
CREATE DATABASE IF NOT EXISTS dbsdupont CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER 'sdupont'@'localhost' IDENTIFIED BY 'XZ3N*@xIQqv0';
GRANT ALL PRIVILEGES ON dbsdupont.* TO 'sdupont'@'localhost';
CREATE DATABASE IF NOT EXISTS dbimarchal CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER 'imarchal'@'localhost' IDENTIFIED BY 'vaD@-@2CrH31';
GRANT ALL PRIVILEGES ON dbimarchal.* TO 'imarchal'@'localhost';
CREATE DATABASE IF NOT EXISTS dbegillet CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER 'egillet'@'localhost' IDENTIFIED BY 'fwl?HdB*Z2r7';
GRANT ALL PRIVILEGES ON dbegillet.* TO 'egillet'@'localhost';
CREATE DATABASE IF NOT EXISTS dbcgay CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER 'cgay'@'localhost' IDENTIFIED BY 'v9eU3riJ-+Kd';
GRANT ALL PRIVILEGES ON dbcgay.* TO 'cgay'@'localhost';
CREATE DATABASE IF NOT EXISTS dbncordier CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER 'ncordier'@'localhost' IDENTIFIED BY '3&LI!1xn0LIk';
GRANT ALL PRIVILEGES ON dbncordier.* TO 'ncordier'@'localhost';
CREATE DATABASE IF NOT EXISTS dbmbuisson CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER 'mbuisson'@'localhost' IDENTIFIED BY '%%@109EqLC9c';
GRANT ALL PRIVILEGES ON dbmbuisson.* TO 'mbuisson'@'localhost';
CREATE DATABASE IF NOT EXISTS dbidossantos CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER 'idossantos'@'localhost' IDENTIFIED BY '36r*F!u%n>MI';
GRANT ALL PRIVILEGES ON dbidossantos.* TO 'idossantos'@'localhost';
CREATE DATABASE IF NOT EXISTS dbelevy CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
CREATE USER 'elevy'@'localhost' IDENTIFIED BY '2g9:+V$BsmfV';
GRANT ALL PRIVILEGES ON dbelevy.* TO 'elevy'@'localhost';
