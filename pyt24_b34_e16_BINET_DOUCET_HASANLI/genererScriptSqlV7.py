# Importation de la module csv et unicode
import csv
import unicodedata
import sys
import subprocess # La module qui permet ed lancer des processus a partir de notre code
import mysql.connector
from datetime import datetime
import tkinter
from tkinter import filedialog
from tkinter import messagebox

######################      Partie Graphique      ##########################################################################################################

class GUI:
    def __init__(self, master):
        self.master = master
        master.title("Générateur de Scripts SQL")
        
        # Variables pour stocker les noms de fichiers
        self.pattern_file = tkinter.StringVar()
        self.users_file = tkinter.StringVar()
        self.output_file = tkinter.StringVar()
        
        # Créer les widgets
        self.create_widgets()
        
    def create_widgets(self):
        
        # Champ de saisie pour le fichier pattern
        tkinter.Label(self.master, text="Fichiers Pattern: ").grid(row=0, column=0)
        tkinter.Label(self.master, textvariable=self.pattern_file).grid(row=0, column=1)
        tkinter.Button(self.master, text="Parcouorir", command=self.load_predefined_patterns).grid(row=0, column=2)
        
        # Champ de saisie pour le fichier users
        tkinter.Label(self.master, text="Fichiers Utilisateurs: ").grid(row=1, column=0)
        tkinter.Label(self.master, textvariable=self.users_file).grid(row=1, column=1)
        tkinter.Button(self.master, text="Parcouorir", command=self.load_predefined_users).grid(row=1, column=2)
        
        # Champ de saisie pour le fichier sortie
        tkinter.Label(self.master, text="Fichiers SQL Généré: ").grid(row=2, column=0)
        tkinter.Entry(self.master, textvariable=self.output_file).grid(row=2, column=1)
        tkinter.Button(self.master, text="Parcouorir", command=self.browse_output).grid(row=2, column=2)
        
        
        # Bouton de lancement
        tkinter.Button(self.master, text="Executer", command=self.generate_and_execute_script, bg="#5b9279", fg="white").grid(row=4, column=0, columnspan=4, pady=10)
        
        # Bouton pour quitter
        tkinter.Button(self.master, text="Quitter",command=self.master.destroy,bg="#c2452d", fg="white").grid(row=5, column=0, columnspan=4, pady=5)
        
        # Configuration de la fenetre
        self.master.grid_rowconfigure(0, weight=1)
        self.master.grid_rowconfigure(1, weight=1)
        self.master.grid_rowconfigure(2, weight=1)
        self.master.grid_rowconfigure(3, weight=1)
        self.master.grid_rowconfigure(4, weight=1)
        self.master.grid_columnconfigure(0, weight=1)
        self.master.grid_columnconfigure(1, weight=1)
        self.master.grid_columnconfigure(2, weight=1)
        
        self.master.geometry("500x200")
        
    # Fonction qui permet de predefinir les fichiers des modeles (patterns)
    def load_predefined_patterns(self):
        predefined_patterns = ["patternSqlCreate.csv","patternSqlDrop.csv"]
        
        pattern_menu = tkinter.OptionMenu(self.master, self.pattern_file, *predefined_patterns)
        
        pattern_menu.grid(row=0, column=1)
        
    # Fonction qui permet de predefinir les fichiers des utilisateurs 
    def load_predefined_users(self):
        predefined_users = ["usersToulouse.csv"]
        
        user_menu = tkinter.OptionMenu(self.master, self.users_file, *predefined_users)
        user_menu.grid(row=1, column =1)

        
    def browse_output(self):
        self.output_file.set(filedialog.asksaveasfilename(defaultextension=".sql", filetypes=[("Fichiers SQL","*.sql")]))
    
    # Fonction qui permet de lancer tout le code avec les fonctions defini
    def generate_and_execute_script(self):
        
        try:
            # Permet lancer la fonction qui va créer le script sql
            creer_script_sql(self.pattern_file.get(), self.users_file.get(), self.output_file.get())
            lancer_script(self.output_file.get())
            enregistrer_en_texte(self.output_file.get())
            messagebox.showinfo("Terminé","Le script SQL a été généré avec succés.")
        except Exception as e:
            messagebox.showerror("Erreur",f"{e}")


            
######################      Creation du Script SQL a la demande      ####################################################################################################

# Fonction qui permet de lire fichier donné en parametre et transformer le contenu dans une liste
def read_csv(file_path):
    with open(file_path, 'r', newline='') as csvfile:
        reader = csv.reader(csvfile)
        return list(reader)

# Fonction pour generer le code sql a partir du pattern choisi
def creer_script_sql(pattern_file, users_file, output_file):
    
    # Fonction qui modifie le nom des utilisateur si il y a des espaces ou des caractéres spéciaux
    def modif_nom(nom):
    # Normalisation des caracteres spéciaux
        nom = unicodedata.normalize("NFKD",nom).encode('ascii','ignore').decode('utf-8')
    # Remplacement des espaces, tirets etc...
        nom = ''.join(ch for ch in nom if ch.isalpha() and ch.isascii()).replace('-','').replace(' ','')
        return nom


# Lecture des utilisateurs depuis usersToulouse.csv et création des login.
    with open(users_file, 'r', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        list_login = [] # Liste pour stocker les logins
        for row in reader:
            prenom = row['givenName'].split('-')
            nom = row['cn']
        # Traiter les prénoms composé
            initials = ''.join(prenom[0] for prenom in prenom)
        # Création des logins
            login = (initials + modif_nom(nom)).lower()
            list_login.append(login)
        
# Lecture des mot de passes créer depuis usersPassword.csv et insertion dans une liste.
    with open('usersPassword2.csv','r',newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        list_mdp = [] # Liste pour stocker les mot de passes
        for row in reader:
            list_mdp.append(row['password'])
    
    # Lecture des modèles depuis le fichier CSV
    patterns = read_csv(pattern_file)

    # Création du script SQL
    with open(output_file, 'w') as sqlfile:
        for idx, user_row in enumerate(list_login):
            user = user_row
            mdp = list_mdp[idx]
            for pattern_row in patterns:
                # Remplacement des motifs XXXXX par les données de l'utilisateur et des motifs PPPPP par les mot de passes
                sql_command = ', '.join(pattern.replace('XXXXX', user).replace('PPPPP',mdp) for pattern in pattern_row)
                sqlfile.write(f"{sql_command}\n")
                
##################### Lancement de script sql a aprtir de code python ##########################################################################


def lancer_script(file):
    
    user = "root"
    pwd = "joliverie"

    commande = f"mysql -u {user} -p{pwd} < {file}"
    
    try :
        # Execution de la commande mysql pour executer le fichier sql en ligne de commande
        subprocess.run(commande,shell=True)
    except :
        # On leve une exception dans le cas ou la commande ne marche pas et on renvoie un message d'erreur
        print("Erreur au lancement :" , subprocess.CalledProcessError)



def enregistrer_en_texte(output_filename):
    date = datetime.now().strftime("%Y_%m_%d__%H_%M")
    script_txt = f"ordres_sql_{date}.txt"
    
    with open(output_filename,'r') as lesScripts, open(script_txt,'w') as sortie:
        sortie.write(lesScripts.read())

           

root = tkinter.Tk()
gui = GUI(root)
root.mainloop()