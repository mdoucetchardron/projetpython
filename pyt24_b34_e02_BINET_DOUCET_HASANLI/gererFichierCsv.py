import csv

## Création de testEntree.csv
file = open('testEntree.csv','w')
file.close()

## Ouverture de testEntree.csv en ecriture et ajout des lignes donnée
with open('testEntree.csv','w',newline='') as csvfile:
    fieldnames = ['first_name','last_name']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    
    writer.writeheader()
    writer.writerow({'first_name':'Emile', 'last_name': 'Zola'})
    writer.writerow({'first_name':'Victor', 'last_name': 'Hugo'})
    writer.writerow({'first_name':'George', 'last_name': 'Sand'})

## Creation de testSortie.csv
file2 = open('testSortie.csv','w')
file2.close()


## ---------- Création Des Logins ----------

## Ouverture de testEntree.csv en lecture pour creer des logins 
# puis inserer dans une liste data_list
with open('testEntree.csv', newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    data_list = []
    for row in reader:
        prenom = row['first_name'][0]
        nom = row['last_name']
        login = (prenom+nom).lower()
        data_list.append(login)

## Ouverture de testSortie.sv en ecriture, 
# on insere le contenu de liste dans le fichier .csv    
with open('testSortie.csv','w',newline='') as csvfile:
    fieldnames = ['login']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    
    writer.writeheader()
    for row in data_list:
        writer.writerow({'login':row})

