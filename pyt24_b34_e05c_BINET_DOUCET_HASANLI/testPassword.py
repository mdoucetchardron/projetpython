import csv
import string
import sys

# Fonction qui validera les conditions 
def validation_password(password):

    # Contraintes du mot de passe
    min_length = 12
    min_lowercase = 2
    min_uppercase = 2
    min_digits = 2
    min_special = 2

    # Vérifier la longueur minimale
    if len(password) < min_length:
        return False, "Longueur insuffisante"

    # Vérifier le nombre de majuscules, minuscules, chiffres, caractères spéciaux
    uppercase_count = sum(1 for char in password if char.isupper())
    lowercase_count = sum(1 for char in password if char.islower())
    digits_count = sum(1 for char in password if char.isdigit())
    special_chars_count = sum(1 for char in password if char in "!@#$%&*+-=.<>/?:")

    messages = []
    
    if uppercase_count < min_uppercase:
         messages.append("Pas assez de majuscules")

    if lowercase_count < min_lowercase:
        messages.append("Pas assez de minuscules")

    if digits_count < min_digits:
        messages.append("Pas assez de chiffres")

    if special_chars_count < min_special:
        messages.append("Pas assez de caractères spéciaux")

    if messages:
        return False, ", ".join(messages)
    else:
        return True, "Mot de passe valide"

#Cette deuxième fonction permettra de vérifier s'il ny'a pas de doublons
def check_password_list(file_path):
    passwords = set()

    with open(file_path, 'r', newline='') as csvfile:
        reader = csv.DictReader(csvfile)

        for row in reader:
            password = row['password'].strip()

            # Vérifier la validité du mot de passe
            is_valid, message = validation_password(password)

            if is_valid:
                # Vérifier les doublons
                if password in passwords:
                    print(f"Alerte : Mot de passe en double trouvé - {password}")
                else:
                    passwords.add(password)
            else:
                print(f"Alerte : Mot de passe non valide - {password} ({message})")

if __name__ == "__main__":
    # Utiliser 'usersPassword2.csv' comme fichier par défaut si aucun argument n'est fourni
    file_path = sys.argv[1] if len(sys.argv) == 2 else 'usersPassword2.csv'
    
    check_password_list(file_path)

    print("Vérification des mots de passe terminée avec succès.")

    # Écrire les alertes dans un fichier CSV
    with open('alerts.csv', 'w', newline='') as alert_file:
        writer = csv.writer(alert_file)
        writer.writerow(["Alerts"])
        for alert in alerts:
            writer.writerow([alert])

    print("Vérification des mots de passe terminée avec succès. Les alertes sont enregistrées dans le fichier 'alerts.csv'.")

