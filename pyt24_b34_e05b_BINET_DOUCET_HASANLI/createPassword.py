import string
import random
import csv

#Première fonction qui de générer un mot de passe avec les conditions précédemment demandées
def generate_password():
    #Caractères autorisés
    uppercase_letters = ''.join(c for c in string.ascii_uppercase if c != 'O') #tous les caractères sont autorisés sauf le 'O'
    lowercase_letters = string.ascii_lowercase
    digits = string.digits
    special_characters = "!@#$%&*+-=.<>/?:"

    #Générer aléatoirement des composants du mot de passe
    uppercase_chars = random.sample(uppercase_letters, k=2)
    lowercase_chars = random.sample(lowercase_letters, k=2)
    digits_chars = random.sample(digits, k=2)
    special_chars = random.sample(special_characters, k=2)

    #Mélange de tous les composants
    all_chars = uppercase_chars + lowercase_chars + digits_chars + special_chars

    #Générer les caractères restants
    remaining_chars = random.sample(uppercase_letters + lowercase_letters + digits + special_characters, k=max(0, 12 - len(all_chars)))

    #Mélange de tous les composants avec les caractères restants générés précédemment 
    all_chars += remaining_chars
    random.shuffle(all_chars)

    password = ''.join(all_chars)
    
    return password

#Deuxième fonction qui écrira le nombre de mot de passe demandés au préalable, dans le fichier CSV 
def create_password_csv(num_passwords):
    # Génération des mots de passe par rapport au nombre de mots de passe demandés
    passwords = [generate_password() for _ in range(num_passwords)]

    # Écriture dans le fichier CSV
    with open('usersPassword2.csv', 'w', newline='') as csvfile:
        fieldnames = ['password']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)

        writer.writeheader()
        for password in passwords:
            writer.writerow({'password': password})

if __name__ == "__main__":
    num_passwords = int(input("Entrez le nombre de mots de passe à générer : "))
    create_password_csv(num_passwords)
    print(f"{num_passwords} mots de passe ont été générés et enregistrés dans le fichier 'usersPassword2.csv'.")
