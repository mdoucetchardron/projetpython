# Imporatation de la module csv et unicode
import csv
import unicodedata
import sys

# Test de nombre de argument passé par parametre
if len(sys.argv) != 4:
    print("Utilisation : python script.py pattern_file input_filename output_filename" )

# On attribut les parametres dans les variables
pattern_file = sys.argv[1]
input_filename = sys.argv[2]
output_filename = sys.argv[3]

# Fonction qui modifie le nom des utilisateur si il y a des espaces ou des caractéres spéciaux
def modif_nom(nom):
    # Normalisation des caracteres spéciaux
    nom = unicodedata.normalize("NFKD",nom).encode('ascii','ignore').decode('utf-8')
    # Remplacement des espaces, tirets etc...
    nom = ''.join(ch for ch in nom if ch.isalpha() and ch.isascii()).replace('-','').replace(' ','')
    return nom


# Lecture des utilisateurs depuis usersToulouse.csv et création des login.
with open(input_filename, 'r', newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    list_login = [] # Liste pour stocker les logins
    for row in reader:
        prenom = row['givenName'].split('-')
        nom = row['cn']
        # Traiter les prénoms composé
        initials = ''.join(prenom[0] for prenom in prenom)
        # Création des logins
        login = (initials + modif_nom(nom)).lower()
        list_login.append(login)
        
# Lecture des mot de passes créer depuis usersPassword.csv et insertion dans une liste.
with open('usersPassword2.csv','r',newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    list_mdp = [] # Liste pour stocker les mot de passes
    for row in reader:
        list_mdp.append(row['password'])

            
######################      Creation du Script SQL a la demande      ###################################

# Fonction qui permet de lire fichier donné en parametre et transformer le contenu dans une liste
def read_csv(file_path):
    with open(file_path, 'r', newline='') as csvfile:
        reader = csv.reader(csvfile)
        return list(reader)

# Fonction pour generer le code sql a partir du pattern choisi
def creer_script_sql(pattern_file, input_filename, output_filename):
    # Lecture des modèles depuis le fichier CSV
    patterns = read_csv(pattern_file)

    # Création du script SQL
    with open(output_filename, 'w') as sqlfile:
        for idx, user_row in enumerate(list_login):
            user = user_row
            mdp = list_mdp[idx]
            for pattern_row in patterns:
                # Remplacement des motifs XXXXX par les données de l'utilisateur et des motifs PPPPP par les mot de passes
                sql_command = ', '.join(pattern.replace('XXXXX', user).replace('PPPPP',mdp) for pattern in pattern_row)
                sqlfile.write(f"{sql_command}\n")

# Permet lancer la fonction qui va créer le script sql
creer_script_sql(pattern_file, input_filename, output_filename)
