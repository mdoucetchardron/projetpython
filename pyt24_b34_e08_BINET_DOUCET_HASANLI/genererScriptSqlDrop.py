import csv
import unicodedata

def modif_nom(nom):
    # Normalisation des caracteres spéciaux
    nom = unicodedata.normalize("NFKD",nom).encode('ascii','ignore').decode('utf-8')
    # Remplacement des espaces, tirets etc...
    nom = ''.join(ch for ch in nom if ch.isalpha() and ch.isascii()).replace('-','').replace(' ','')
    return nom

with open('usersToulouse.csv','r', newline='') as csvfile:
    reader = csv.DictReader(csvfile)
    list_login = []
    for row in reader:
        prenom = row['givenName'].split('-')
        nom = row['cn']
        # Traiter les prénoms composé
        initials = ''.join(prenom[0] for prenom in prenom)
        # Création des logins
        login = (initials + modif_nom(nom)).lower()
        list_login.append(login)
        

with open('supprimerUsersBddAcces.sql', 'w') as sqlfile:
    # Ecriture des requetes SQL pour création d'utilisateur.
    for user in list_login:
            login = user
            sqlfile.write(f"DROP DATABASE IF EXISTS db{login};\n")
            sqlfile.write(f"DROP USER '{login}'@'localhost';\n")